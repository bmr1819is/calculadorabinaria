package com.example.calculadorabinaria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private CheckBox check0, check1, check2, check3, check4, check5, check6, check7, check8, check9;
    private TextView textResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.check0=findViewById(R.id.check0);
        this.check1=findViewById(R.id.check1);
        this.check2=findViewById(R.id.check2);
        this.check3=findViewById(R.id.check3);
        this.check4=findViewById(R.id.check4);
        this.check5=findViewById(R.id.check5);
        this.check6=findViewById(R.id.check6);
        this.check7=findViewById(R.id.check7);
        this.check8=findViewById(R.id.check8);
        this.check9=findViewById(R.id.check9);
        this.textResultado = findViewById(R.id.textResultado);

        check0.setOnCheckedChangeListener(this);
        check1.setOnCheckedChangeListener(this);
        check2.setOnCheckedChangeListener(this);
        check3.setOnCheckedChangeListener(this);
        check4.setOnCheckedChangeListener(this);
        check5.setOnCheckedChangeListener(this);
        check6.setOnCheckedChangeListener(this);
        check7.setOnCheckedChangeListener(this);
        check8.setOnCheckedChangeListener(this);
        check9.setOnCheckedChangeListener(this);

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    int sumas=0, var=0;
    String Res;

    var += check0.isChecked() ? 1: 0;
    var += check1.isChecked() ? 2: 0;
    var += check2.isChecked() ? 4: 0;
    var += check3.isChecked() ? 8: 0;
    var += check4.isChecked() ? 16: 0;
    var += check5.isChecked() ? 32: 0;
    var += check6.isChecked() ? 64: 0;
    var += check7.isChecked() ? 128: 0;
    var += check8.isChecked() ? 256: 0;
    var += check9.isChecked() ? 512: 0;
    sumas=sumas+var;
    Res=String.valueOf(sumas);
    textResultado.setText(Res);
    }
}
